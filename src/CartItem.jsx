import React from 'react';
import PropTypes from 'prop-types';
import './CartItem.css';

const CartItem = (props) => {
  const { changeQuantity, info, checkout, removeFromCart} = props;
  const {productDetails, quantity} = info;

  return (
    <div styleName={checkout ? 'cartItemContainerCheckout' : 'cartItemContainer'}>
      <div styleName="cartItemInfo">
        <img src={productDetails.image} alt={productDetails.title} styleName={checkout ? 'cartInfo cartInfoCheckout' : 'cartInfo'} />
        <div styleName="quantityContainer cartInfo">
          <p styleName="quantity">
            {checkout ? quantity : <input onChange={(e) => {
              const value = e.target.value
              changeQuantity(productDetails.id, value)
            } } styleName="quantityInput" value={quantity} />}
          </p>
        </div>
        <div styleName="priceContainer cartInfo">
          <p styleName="price">
$
            {productDetails.price}
          </p>
        </div>
        {!checkout && <div onClick={() => removeFromCart(productDetails.id)} styleName="remove">Remove</div>}
      </div>
    </div>
  );
}

CartItem.propTypes = {
  info: PropTypes.shape({
    id: PropTypes.number,
    image: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
  }).isRequired,
  checkout: PropTypes.bool.isRequired,
  quantity: PropTypes.number,
};

export default CartItem;
