import { combineReducers } from 'redux';
import {TOGGLE_CHECKOUT, TOGGLE_CART, ADD_TO_CART, CLEAR_CART, REMOVE_FROM_CART, SORT_BY, CHANGE_ITEM_QUANTITY} from './actions';

const toggleCheckout = (state = false, action) => {
  switch (action.type) {
    case TOGGLE_CHECKOUT:
      return action.display;
    default:
      return state;
  }
}

const toggleCart = (state = false, action) => {
  switch (action.type) {
    case TOGGLE_CART:
      return !state;
    default:
      return state;
  }
}

const itemsReducer = (state = {itemsInCart: [], itemsObj: {}, itemsArr: []}, action) => {
  switch (action.type) {
    case ADD_TO_CART: 
      return Object.assign({}, state, {itemsInCart: state.itemsInCart.concat({productDetails: state.itemsObj[action.id], quantity: '1'})})
    case REMOVE_FROM_CART:
    return Object.assign({}, state, {itemsInCart: state.itemsInCart.filter((itemInCart) => {
      return itemInCart.productDetails.id !== action.id;  
    }) });
    case CLEAR_CART: {
      return Object.assign({}, state, {itemsInCart: []})
    }
    case CHANGE_ITEM_QUANTITY: {
      return Object.assign({}, state, {itemsInCart: state.itemsInCart.map((itemInCart) => {
        if (itemInCart.productDetails.id === action.id) {
          return Object.assign({}, itemInCart, {quantity: action.quantity})
        }
        return itemInCart; } )
      })
    }
    default:
      return state;
  }
}

const sortBy = (state = 'lowest-to-highest', action) => {
  switch (action.type) {
    case SORT_BY:
      return state === 'lowest-to-highest' ? 'highest-to-lowest' : 'lowest-to-highest';
    default:
      return state;
  }
}

export default combineReducers({
  toggleCheckout,
  toggleCart,
  itemsReducer,
  sortBy,
})