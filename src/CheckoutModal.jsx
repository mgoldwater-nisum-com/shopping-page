import React from 'react';
import {connect} from 'react-redux';
import {toggleCheckout} from './actions';
import CartItem from './CartItem';
import Total from './Total';
import './CheckoutModal.css';

const CheckoutModal = (props) => {
  const { dispatch, itemsInCart } = props;
  return (
    <div styleName="checkoutModal">
      <div onClick={() => dispatch(toggleCheckout(false)) } styleName="close">X</div>
      <h3 styleName="checkoutHeading">Your order is:</h3>
      <div styleName="checkoutItems">
        {itemsInCart.map(item => (
          <CartItem key={item.id} info={item} checkout />
        ))}
      </div>
      <Total />
    </div>
  );
};

const mapStateToProps = (state) => {
  const {itemsReducer} = state
  return {itemsInCart: itemsReducer.itemsInCart}
}

export default connect(mapStateToProps)(CheckoutModal);
