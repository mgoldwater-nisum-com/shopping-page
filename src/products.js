import shirt0 from '../public/images/shirt_0.jpg';
import shirt1 from '../public/images/shirt_1.jpg';
import shirt2 from '../public/images/shirt_2.jpg';
import shirt3 from '../public/images/shirt_3.jpg';
import shirt4 from '../public/images/shirt_4.jpg';
import shirt5 from '../public/images/shirt_5.jpg';

export default [
  {
    id: 0,
    image: shirt0,
    title: 'Goodthreads White T-Shirt',
    price: 5.49,
  },
  {
    id: 1,
    image: shirt1,
    title: 'Amazon Black T-Shirt',
    price: 10.99,
  },
  {
    id: 2,
    image: shirt2,
    title: 'Goodthreads Blue V Neck',
    price: 15.99,
  },
  {
    id: 3,
    image: shirt3,
    title: 'Amazon Purple & White Buttoned Down',
    price: 19.99,
  },
  {
    id: 4,
    image: shirt4,
    title: 'Goodthreads Men\'s Standard-Fit Long-Sleeve Micro-Check Shirt',
    price: 24.99,
  },
  {
    id: 5,
    image: shirt5,
    title: 'Goodthreads Standard Fit Long Sleeve Gingham Shirt',
    price: 29.99,
  },
];