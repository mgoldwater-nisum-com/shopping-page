import React from 'react';
import { hot } from 'react-hot-loader/root';
import './App.css';
import HomepageContainer from './HomepageContainer';

const App = () => (
  <HomepageContainer />
);
export default hot(App);
