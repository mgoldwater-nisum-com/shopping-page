import Cart from './Cart';
import {connect} from 'react-redux';
import {clearCart, toggleCart, toggleCheckout} from './actions';

const mapStateToProps = (state) => {
  const {itemsReducer, toggleCart} = state;
  return {cartSelected: toggleCart, items: itemsReducer.itemsInCart}
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearCart: () => dispatch(clearCart()),
    toggleCart: () => dispatch(toggleCart()),
    toggleCheckout: () => dispatch(toggleCheckout(true))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);