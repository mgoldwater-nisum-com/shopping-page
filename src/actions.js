export const TOGGLE_CHECKOUT = 'TOGGLE_CHECKOUT';
export const TOGGLE_CART = 'TOGGLE_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
export const CLEAR_CART = 'CLEAR_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const SORT_BY = 'SORT_BY';
export const CHANGE_ITEM_QUANTITY = 'CHANGE_ITEM_QUANTITY';

export const toggleCheckout = (display) => {
  return {type: TOGGLE_CHECKOUT, display}
}

export const toggleCart = () => {
  return {type: TOGGLE_CART}
}

export const addToCart = (id) => {
  return {type: ADD_TO_CART, id}
}

export const clearCart = () => {
  return {type: CLEAR_CART}
}

export const removeFromCart = (id) => {
  return {type: REMOVE_FROM_CART, id}
}

export const sortBy = () => {
  return {type: SORT_BY}
}

export const changeItemQuantity = (id, quantity) => {
 return {type: CHANGE_ITEM_QUANTITY, id, quantity}
}
