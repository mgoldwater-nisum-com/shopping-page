import React from 'react';
import './Homepage.css';
import CartContainer from './CartContainer';
import Item from './Item';
import CheckoutModal from './CheckoutModal';

const Homepage = (props) => {
  const {items, onSortByClick ,sortBy, toggleCheckout} = props;
  return (
    <div styleName="Homepage">
      <h1 styleName="title">Shopping Page!</h1>
      <div styleName="sortButton">
        <button onClick={onSortByClick} type="button">
          <div styleName="sortItems">Sort Items</div>
          {sortBy === 'lowest-to-highest' ? <div styleName="arrowUp" /> : <div styleName="arrowDown" />}
        </button>
      </div>
      <CartContainer allItems={items} />
      <div styleName="itemsContainer">
        {items.map(item => (
          <Item key={item.id} item={item} />
        ))}
      </div>
      <div styleName="footer" />
      {toggleCheckout && <CheckoutModal checkout />}
    </div>
  );
}

export default Homepage;
