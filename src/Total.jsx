import React from 'react';
import {connect} from 'react-redux';
import './Total.css';

const Total = (props) => {
  const {total} = props;
  return (
    <div styleName="totalDisplay">
      <p styleName="totalElement totalText">Total:</p>
      <p styleName="totalElement totalNumber">${total}</p>
    </div>
  );
}

const calculateTotal = (items) => {
  let total = 0;
  items.forEach((item) => {
    total += item.productDetails.price * item.quantity;
  });
  return total;
}

const mapStateToProps = (state) => {
  const {itemsReducer} = state;
  return {total: calculateTotal(itemsReducer.itemsInCart)}
}

export default connect(mapStateToProps,null)(Total);
