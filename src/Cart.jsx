import React from 'react';
import './Cart.css';
import PropTypes from 'prop-types';
import CartItemContainer from './CartItemContainer';
import Total from './Total';

const Cart = (props) => {
  const { items, cartSelected, clearCart, toggleCart, toggleCheckout } = props;
  return (
    <div styleName={cartSelected ? 'itemsAndCart itemsAndCartWhenSelected' : 'itemsAndCart itemsAndCartNotSelected'}>
      <div styleName={cartSelected ? 'itemsInCart' : 'itemsInCart'}>
        <p styleName="itemsInCartNumber">{items.length}</p>
      </div>
      <div styleName={cartSelected ? 'cartContainerWhenSelected' : 'cartContainerNotSelected'}>
        <div onClick={toggleCart} styleName="Cart">Cart</div>
      </div>
      {cartSelected && (
      <div styleName="cartDisplayContainer">
        {items.map(item => {
          return <CartItemContainer key={item.productDetails.id} info={item} checkout={false} />
        })}
        <Total />
        <div styleName="clearAndCheckoutContainer">
          <div onClick={clearCart} styleName="clearCart">Clear Cart</div>
          <div onClick={toggleCheckout} styleName="checkout">Checkout</div>
        </div>
      </div>
      )}
    </div>
  );
}
Cart.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  cartSelected: PropTypes.bool.isRequired,
  toggleCart: PropTypes.func.isRequired,
  allItems: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Cart;
