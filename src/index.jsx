import React from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import ReactDOM from 'react-dom';
import App from './App';
import 'normalize.css';
import products from './products';

import rootReducer from './reducers'

const itemsObj = {0: products[0], 1: products[1], 2: products[2], 3: products[3], 4: products[4], 5: products[5]}

const store = createStore(rootReducer, {itemsReducer: {itemsInCart: [], itemsObj, itemsArr: [itemsObj[0], itemsObj[1], itemsObj[2], itemsObj[3], itemsObj[4], itemsObj[5]]}});
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
, 
document.getElementById('root'));
