import {connect} from 'react-redux';
import {sortBy} from './actions';
import Homepage from './Homepage';

const sortItems = (sortBy, items) => {
  if (sortBy === 'lowest-to-highest') {
    items.sort((a,b) => {
      return a.price - b.price
    })
  } else {
    items.sort((a,b) => {
      return b.price - a.price
    })
  }
  return items;
}

const mapStateToProps = (state) => {
  const {sortBy, itemsReducer, toggleCheckout} = state;
  return {items: sortItems(sortBy, itemsReducer.itemsArr), sortBy, toggleCheckout}
}

const mapDispatchToProps = (dispatch) => {
    return {
      onSortByClick: () => dispatch(sortBy())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Homepage);