import {mount} from 'enzyme';
import React from 'react';
import {Provider} from 'react-redux';
import App from './App';
import shirt1 from '../public/images/shirt_1.jpg';
import products from './products';
import {createStore} from 'redux';
import rootReducer from './reducers'

const makeStore = () => {
  const itemsObj = {0: products[0], 1: products[1], 2: products[2], 3: products[3], 4: products[4], 5: products[5]}
  const store = createStore(rootReducer, {toggleCheckout: false, toggleCart: false, sortBy: 'lowest-to-highest', itemsReducer: {itemsInCart: [], itemsObj, itemsArr: [itemsObj[0], itemsObj[1], itemsObj[2], itemsObj[3], itemsObj[4], itemsObj[5]]}});
  return store;
}

const clickToAddItemToCart = (wrapper) => {
  const addToCartButtons = wrapper.find('.addToCartButton');
  addToCartButtons.at(1).simulate('click');
}

describe('sorts items by price', () => {
  it('sorts the items from most expensive to cheapest when the sort items button is clicked with a down arrow and sorts the items from most expensive to least expensive when sort items is clicked with an up arrow', () => {
    const store = makeStore();
    const wrapper = mount(<Provider store={store}>
      <App />
    </Provider>);
    expect(store.getState().sortBy).toBe('lowest-to-highest');
    const productsCopy = Object.assign([], products)
    const button = wrapper.find('button');
    button.simulate('click');
    expect(store.getState().itemsReducer.itemsArr).toEqual(productsCopy.reverse());
    expect(store.getState().sortBy).toBe('highest-to-lowest');
    wrapper.find('button').simulate('click');
    expect(store.getState().itemsReducer.itemsArr).toEqual(productsCopy.reverse());
    expect(store.getState().sortBy).toBe('lowest-to-highest');
    wrapper.unmount();
  })
})

describe('shopping cart', () => {
  it('does not display the shopping cart when the page loads and toggles the cart upon clicking the cart div', () => {
    const store = makeStore();
    const wrapper = mount(<Provider store={store}> 
      <App />
    </Provider>);
    const cart = wrapper.find('.Cart');
    expect(cart).toHaveLength(1);
    expect(store.getState().toggleCart).toBe(false);
    cart.at(0).simulate('click');
    expect(store.getState().toggleCart).toBe(true);
    cart.at(0).simulate('click');
    wrapper.unmount();
  })

  it('adds an item to the shopping cart and increases the cart quantity by 1 when add to cart is clicked', () => {
    const store = makeStore();
    const wrapper = mount(<Provider store={store}> 
      <App />
    </Provider>);
    const cart = wrapper.find('.Cart');
    cart.at(0).simulate('click');
    const addToCartButtons = wrapper.find('.addToCartButton');
    expect(addToCartButtons).toHaveLength(6);
    expect(store.getState().itemsReducer.itemsInCart).toEqual([]);
    addToCartButtons.at(1).simulate('click');
    expect(store.getState().itemsReducer.itemsInCart).toEqual([{productDetails: {
      id: 1,
      image: shirt1,
      title: 'Amazon Black T-Shirt',
      price: 10.99,
    }, quantity: '1'
    }]);
    const remove = wrapper.find('.remove');
    remove.first().simulate('click');
    cart.at(0).simulate('click');
    wrapper.unmount();
  })
})

describe('removing items from the cart', () => {
  it('removes an item from the cart when the remove button is clicked and the price is adjusted accordingly', () => {
    const store = makeStore();
    const wrapper = mount(<Provider store={store}> 
      <App />
    </Provider>);
    console.log('store.getState is: ', store.getState());
    clickToAddItemToCart(wrapper);
    const cart = wrapper.find('.Cart');
    cart.at(0).simulate('click');
    const remove = wrapper.find('.remove');
    remove.first().simulate('click');
    expect(store.getState().itemsReducer.itemsInCart).toEqual([]);
    wrapper.unmount();
  })

  it('empties the cart when the user clicks clear cart', () => {
    const store = makeStore();
    const wrapper = mount(<Provider store={store}> 
      <App />
    </Provider>);
    clickToAddItemToCart(wrapper);
    const cart = wrapper.find('.Cart');
    cart.at(0).simulate('click');
    const clearCart = wrapper.find('.clearCart');
    clearCart.at(0).simulate('click');
    expect(store.getState().itemsReducer.itemsInCart).toEqual([]);
    wrapper.unmount();
  })
})

describe('checkout modal', () => {
  it('displays the checkout modal upon clicking checkout and exits the checkout modal upon clicking the x in the checkout', () => {
    const store = makeStore();
    const wrapper = mount(<Provider store={store}> 
      <App />
    </Provider>);
    // console.log('store.getState() is: ', store.getState());
    expect(store.getState().toggleCheckout).toBe(false);
    clickToAddItemToCart(wrapper);
    const cart = wrapper.find('.Cart');
    cart.at(0).simulate('click');
    const checkout = wrapper.find('.checkout');
    checkout.at(0).simulate('click');
    expect(store.getState().toggleCheckout).toBe(true);
    const close = wrapper.find('.close');
    close.at(0).simulate('click');
    expect(store.getState().toggleCheckout).toBe(false);
    wrapper.unmount();
  })
})

it('should change the listed item quantity upon user input and update the total price', () => {
  const store = makeStore();
  const wrapper = mount(<Provider store={store}> 
    <App />
  </Provider>);
  clickToAddItemToCart(wrapper);
  const cart = wrapper.find('.Cart');
  cart.at(0).simulate('click');
  const quantityInput = wrapper.find('.quantityInput');
  quantityInput.simulate('change', {target: {value: '2'}});
  expect(store.getState().itemsReducer.itemsInCart).toEqual([{productDetails: {
    id: 1,
    image: shirt1,
    title: 'Amazon Black T-Shirt',
    price: 10.99,
  }, quantity: '2'
  }]);
  const totalNumber = wrapper.find('.totalNumber');
  expect(totalNumber.text()).toBe('$21.98');
})