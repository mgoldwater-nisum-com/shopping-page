import React from 'react';
import {connect} from 'react-redux';
import {addToCart} from './actions';
import PropTypes from 'prop-types';
import './Item.css';


const Item = (props) => {
  const { dispatch, item } = props;

  return (
    <div styleName="itemParent">
      <div styleName="itemContainer">
        <div>
          <img styleName="itemImage" src={item.image} alt={item.title} />
          <p styleName="itemTitle item">{item.title}</p>
          <p styleName="itemPrice item">
$
            {item.price}
          </p>
          <div onClick={() => dispatch(addToCart(item.id))} styleName="addToCartButton item">Add to Cart</div>
        </div>
      </div>
    </div>
  );
};

Item.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number,
    image: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
  }).isRequired,
};

export default connect()(Item);
