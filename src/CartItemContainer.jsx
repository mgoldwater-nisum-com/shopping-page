import {connect} from 'react-redux';
import {changeItemQuantity ,removeFromCart} from './actions';
import CartItem from './CartItem';

const mapDispatchToProps = (dispatch) => {
  return {
    changeQuantity: (id, quantity) => dispatch(changeItemQuantity(id, quantity)),
    removeFromCart: (id) => dispatch(removeFromCart(id))
  }
}

export default connect(null, mapDispatchToProps)(CartItem);